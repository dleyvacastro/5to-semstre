function [x1, x2,  x3] = phinv(z)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    x1 = (z+ conj(z))/(abs(z)^2 +1);
    x2 = (-i*(z-conj(z)))/((abs(z)^2 +1));
    x3 = (abs(z)^2 -1)/(abs(z)^2 +1);
end