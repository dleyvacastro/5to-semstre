function result = phi(x1, x2, x3)
x = [x1 x2 x3];
if round(norm(x)) == 1
    x1 = sym('x1', 'real');
    x2 = sym('x2', 'real');
    x3 = sym('x3', 'real');
    %syms x1 x2 x3
    phi1(x1, x2, x3) = (x1 + i*x2)/(1-x3);
    result = phi1(x(1), x(2), x(3));
else
    disp(norm(x))
    disp('Dominio incorrecto');
end
end