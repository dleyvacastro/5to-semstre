%1  
disp('p1') 
z1 = 2+3i 
z2 = 4+i 
z3 = 1+2i 
z4=3-4i 
z5 = 2-i 
z6 = 5i 
 
% 2 
disp('p2') 
z1+z2 
z1/z2 
z3/z4 + z5/z6 
z1*z2 
 
% 3 
disp('p3') 
real(z1) 
real(z2) 
real(z3) 
real(z4) 
 
 
% 4 
disp('p4')
imag(z5) 
imag(z6) 
imag(z1/z2) 
imag(z4*z6) 
 
% 5 
conj((3+8i)/(1+i)) 
conj((i*(2+3i)*(5-2i))/(-2-i)) 
conj(((2-3i)^2)/((8+6i)^2)) 
conj((1+i)/(1-i)) 
 
% 6 
abs(3+8i) 
abs(1+i) 
abs(5-2i) 
abs(8+6i) 
abs(2-3i) 
 
% 7 
angle(1+i) 
angle(-1+i) 
angle(1+i/2) 
angle(1-i) 
 
% 8

a8 = cos(pi/4) +i*sin(pi/4)
b8 = cos(pi/2)+i*sin(pi/2)
c8 = cos(pi)+i*sin(pi)
d8 = 1/3 + i * sqrt(2)

%z = [a8 b8 c8 d8]
axis square;
grid on;

plot(real(a8), imag(a8), '*', real(b8), imag(b8), '+', real(c8), imag(c8), 'o', real(d8), imag(d8), 'x')
legend({'a','b','c','d'},'Location','southwest') ; 
xlabel('real');
ylabel('imag');
% 9
a9 = 1+i
b9 = -1+i
c9 = -1/2+i
d9 = 1-i

r =real([a9, b9, c9, d9])
i = imag([a9, b9, c9, d9])

c = compass(r, i)
legend({'a','b','c','d'},'Location','southwest') ; 
a9l = c(1)
b9l = c(2)
c9l = c(3)
d9l = c(4)

a9l.Color = 'r'
b9l.Color = 'g'
c9l.Color = 'b'
d9l.Color = 'c'
%compass(a9, 'r', b9, 'g')
%10
a10 = sqrt(2)*exp(i*pi/4)
b10 = sqrt(2)*exp(i*3*pi/4)
c10 = sqrt(2)*exp(i*-3*pi/4)
d10 = sqrt(2)*exp(i*-pi/4)