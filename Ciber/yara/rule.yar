rule putty_dep{
    strings:
        $dev = "SimonTatham" ascii
        $putty = "putty"
        $ssh = "ssh"

    condition:
        (uint16(0) == 0x5A4D) and (filesize < 2MB) and (all of them)
}
