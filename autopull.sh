#!/bin/bash
d=$(date)

if git -C /home/dleyvacastro/Documentos/UR/5to\ Semestre/ pull; then
    echo "Repo pulled at $d" >> log.txt
    notify-send "Pulled succed"
else
    echo "Pull Fail at $d" >> /home/dleyvacastro/Documentos/UR/5to\ Semestre/log.txt
    notify-send "Pull failed"
fi

