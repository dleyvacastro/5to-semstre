---
title: "t4AED"
author: "Daniel Leyva"
date: "3/11/2022"
output: html_document
---


# 1
## a)
```{r}
xb <- cbind(46.1,57.3,50.4)
xb = t(xb)
S <- cbind(c(101.3,63,71), c(63,80.2,55.6), c(71,55.6,97.4))
C = cbind(c(1,0),c(-1,1),c(0,-1))
q <- 3
n <- 40
Ts = (n*t(C%*%xb)%*%solve(C%*%S%*%t(C))%*%(C%*%xb))
print(Ts)


Fa = ((n-1)*(q-1)/(n-q+1))*3.24482 # F(q-1,n-q+1)
print(Fa)
```
$T^2 = 90.4945$
$F = 6.66042$
Note que $90.4945 > 6.6642$, por lo tanto se rechaza $H_0$.
## b)
```{r}
c1 = rbind(1,-1,0)
c2 = rbind(1,0,-1)
c3 = rbind(0,1,-1)

Is1 = t(c1) %*% xb + sqrt(Fa)*sqrt((t(c1)%*%S%*%c1)/(n))
Ii1 = t(c1) %*% xb - sqrt(Fa)*sqrt((t(c1)%*%S%*%c1)/(n))

Is2 = t(c2) %*% xb + sqrt(Fa)*sqrt((t(c2)%*%S%*%c2)/(n))
Ii2 = t(c2) %*% xb - sqrt(Fa)*sqrt((t(c2)%*%S%*%c2)/(n))

Is3 = t(c3) %*% xb + sqrt(Fa)*sqrt((t(c3)%*%S%*%c3)/(n))
Ii3 = t(c3) %*% xb - sqrt(Fa)*sqrt((t(c3)%*%S%*%c3)/(n))

```
$$I_1 = (-14.2, -8.16)$$
$$I_2 = (-7.37, -1.23)$$
$$I_3 = (3.57, 10.2)$$
# 2
## a)
```{r}
c1 <- c(3,1,2)
c2 <- c(3,6,3)
t1 <- cbind(c1, c2)

t2 = cbind(c(2,5,3,2), c(3,1,1,3))

s1 <- cov(t1)
s2 <- cov(t2)

n1 <- 3
n2 <- 4

Spooled <- ((n1 - 1)*s1 + (n2 - 1)*s2) / (n1+n2-2)
```
$S_{pooled} = \begin{bmatrix} 1.6 & -1.4 \\ -1.4 & 2 \end{bmatrix}$
## b)
```{r}
xb1 = c(mean(c1), mean(c2))
xb2 = c(mean(c(2,5,3,2)), mean(c(3,1,1,3))) 
cs = ((n1+n2-2)*2/(n1+n2-2-1))*99
Ts = t(xb1 -xb2)%*%solve((1/n1 + 1/n2)*Spooled)%*%(xb1 -xb2)
print(cs)
print(Ts)
# No se rechaza
``` 
247.5 > 3.870968
Por lo tanto no se rechaza

## c)
```{r}
a = rbind(c(1), c(0))
I1s = t(a)%*%(xb1 -xb2)+sqrt(cs*t(a)%*%((1/n1 + 1/n2)*Spooled)%*%a)
I1i = t(a)%*%(xb1 -xb2)-sqrt(cs*t(a)%*%((1/n1 + 1/n2)*Spooled)%*%a)

a = rbind(c(0), c(1))
I2s = t(a)%*%(xb1 -xb2)+sqrt(cs*t(a)%*%((1/n1 + 1/n2)*Spooled)%*%a)
I2i = t(a)%*%(xb1 -xb2)-sqrt(cs*t(a)%*%((1/n1 + 1/n2)*Spooled)%*%a)
```

# 3
## a)
```{r}
z1 = c(10,5,7,19,11,18)
z2 = c(2,3,3,6,7,9)
y = c(15,9,3,25,7,13)
Zg = cbind(c(1,1,1,1,1,1), z1, z2)
#z3 = z1^2
model <- lm(y ~ z1+z2)

summary(model)
```
## b)
```{r}
n = 6
r = 2
bg = solve(t(Zg)%*%Zg)%*%t(Zg)%*%y

yg = Zg%*%bg
eg = y-yg
Ss = (t(eg)%*%eg)/(n-r-1)
Ss = det(Ss) # Ninguna formula, solo que r es raro
varb = Ss*solve(t(Zg)%*%Zg)
# Intervalos para b1
I1s = bg[2]+sqrt(varb[5])*sqrt((r+1)*9.28) # F3,3
I1i = bg[2]-sqrt(varb[5])*sqrt((r+1)*9.28)
# Intervalos para b2
I2s = bg[3]+sqrt(varb[9])*sqrt((r+1)*9.28) # F3,3
I2i = bg[3]-sqrt(varb[9])*sqrt((r+1)*9.28)
```
## c)

````{r}
q = 1
z0 = cbind(c(1,1,1,1,1,1), z2)
bg0 = rbind(bg[1], bg[3])
ess = t(y - z0 %*% bg0)%*%(y - z0 %*% bg0)-t(y-Zg%*%bg)%*%(y-Zg%*%bg)
ess = det(ess)
Ss0 = (t(y-Zg%*%bg)%*%(y-Zg%*%bg))/(n-r-1)
Ss0 = det (Ss0)
h0 = (ess)/((r-q)/(Ss0))

```
