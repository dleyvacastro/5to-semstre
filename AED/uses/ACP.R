#C.P - Eigenvals&vecs ----
## Si no son estandarizadas se usa Sigma
Sigma <- matrix(c(5,2,2,2), nrow = 2,byrow = TRUE)
cov_eigen <- eigen(Sigma)

Y1 <- cov_eigen$vectors[,1]
Y2 <- cov_eigen$vectors[,2]
L1 <- cov_eigen$values[1]
L2 <- cov_eigen$values[2]


##Si estan estandarizadas usar R

###Si nos dan la S pero nos piden convertirla a S, sacar diag y operar con S
V<-sqrt(diag(diag(S)))
R<- solve(V) %*% S %*% solve(V)
###Si nos dan R solo ponerla aqui abajo
#R<- matrix(c(?,?,?,?), nrow= num_rows, byrow=TRUE)


R_eigen <- eigen(R)


# Proporc. ----

#sumar las los val propios que tengamos en el denom: prop_k=Lk/(L1+...+Lp)
prop_1 <- L1/(L1+L2)

#Si quiero la prop de 2 componentes al tiempo con relaci�n a las dem�s,
#sumar las los val propios que tengamos en el denom: prop_ki=(Lk+Li)/(L1+...+Lp)
#prop_ki<-(Lk+Li)/(L1+...+Lp)

#Correlaci�n de una variable con la comp ----

#si no se estandariza
rho_yi_xk <- Y1[1]*sqrt(L1)/(sqrt(S[1,1]))

#si se estandariza
#rho_yi_xk <- Y1[1]*sqrt(L1)
