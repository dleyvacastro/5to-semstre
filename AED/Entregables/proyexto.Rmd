---
title: "Proyecto AED"
author: "Andres Diaz, Camilo Fernandez, Daniel Amaya, Daniel Leyva"
output:
  pdf_document: default
  html_document: default
  word_document: default
date: '2022-05-13'
---
```{r include=FALSE, results=FALSE}
library(dplyr)
#library(ggplot2)
library(Hmisc)

library(corrplot)
knitr::opts_chunk$set(echo = TRUE)


base100 = read.csv('water_potability.csv')
water <- base100

```
# Introducción
El agua es un recurso natural indispensable para la vida en el planeta tierra, por esto, todas las personas deberían tener acceso a agua limpia y potable. Este recurso es fundamental en muchos factores de la vida humana, especialmente en la salud. Debido a diferentes condiciones el agua puede contener sustancias o elementos que son perjudiciales; aun así, cuando se tienen valores óptimos de algunas sustancias en el agua, puede considerarse segura para el consumo, garantizando que no habrá enfermedades ni efectos negativos cuando se la utilice. 

# Objetivo
Nuestro objetivo al analizar esta base de datos es poder encontrar una relación entre las diferentes variables presentadas con el fin de poder predecir en qué condiciones una muestra de agua puede ser segura para el consumo humano; así mismo, se desean ver las relaciones de dependencia e independencia presentes en estas variables, para ver cuáles son más propensas a generar un cambio significativo en la calidad del agua.  

# Variables

- **pH:**
El valor pH es un parámetro que indica la acidez o alcalinidad del agua, lo cual influye en su potabilidad.
- **Dureza: **
La dureza del agua indica la cantidad de calcio y magnesio disueltos en esta. Esto pese a no estar considerado como un riesgo para la salud, si se considera una molestia en las labores domésticas.
- **Solidos:**
Este atributo representa el total de elementos sólidos disueltos. Se mide en partes por millón.
- **Cloraminas:**
Las cloraminas son compuestos químicos utilizados para mejorar el olor y sabor del agua potable, como también para su desinfección. Esta variable mide la cantidad de cloraminas en partes por millón. 
- **Sulfato**
Este se refiere a los sulfatos, los cuales se encuentran en varios lugares de forma natural, entre estos, el agua. La concentración de este normalmente es de 2700 miligramos por litro pero puede ser variante y, en algunas regiones, puede llegar a ser mucho más elevado.
    
- **Conductividad**
La conductividad en el agua es un buen indicador de su pureza, pues cuando ocurre el caso de que es pura, esta se convierte en un buen aislante en lugar de un buen conductor, debido a que la cantidad de solidos que estén disueltos en el agua mejora la conductividad de esta.
    
- **Carbónico Orgánico Total (TOC):** Esta hace referencia a una medida que calcula la cantidad total de carbón en compuestos orgánicos de agua pura.   
    
- **Trihalometanos:** Son sustancias que se encuentran en agua tratada con cloro, esta varía según el nivel de materia orgánica en el agua, la cantidad de cloro, y la temperatura.
    
- **Turbidez del agua:** Medida de las propiedades emisoras de luz del agua, y la prueba de esta es usada para indicar la calidad de la descarga de desechos con respecto a la materia coloidal. 
    
- **Potabilidad: ** Indica si el agua es segura para el consumo humano.

# 1. Regresión lineal
## Procedimiento y resultados
```{r}

potability <- water$Potability
pob0 <- subset(water, potability==0)
pob1 <- subset(water, potability==1)
```
En primer, lugar se estudió la correlación de las variables para entender el comportamiento y las relaciones existentes entre las mismas. Como puede verse en la matriz, la mayoría de las correlaciones tienen un color blanco, lo que indica que su valor es muy cercano a cero. Algunas variables tienen una correlación ligeramente diferente, como "Solids" y "Sulfate", pero sigue siendo baja. Esto nos indica que muy posiblemente no se obtendrá un buen desempeño en los modelos lineales para explicar las variables.

```{r echo=FALSE}
corr <- cor(water)
corrplot(corr, number.cex = 0.6)
```

Se realizaron modelos de regresión lineal para cada una de las variables del conjunto de datos. El valor del R-squared de estos modelos era notablemente bajo, y el valor más alto que se logró obtener fue 0.03 aproximadamente. Esto indica que solamente se logra explicar un 3% de la variabilidad de los datos con la regresión, lo que no es muy bueno. Inicialmente se tomaron todas las variables como predictores en cada modelo, incluyendo la variable potabilidad y por medio de una regresión stepwise se obtuvo el modelo óptimo. 

Uno de estos modelos corresponde al de la variable "Sulfate", que como puede verse tiene un R-squared de 0.034. Las variables predictoras son "Solids", "Hardness", "Organic_carbon" y "Trihalomethanes", que tienen un p-valor bajo, indicando que son significativamente diferentes de cero dentro del modelo. Se puede observar que la variable "Potability" que es una de la más importantes dentro del conjunto de datos no tiene importancia aquí. De hecho, en general esta variable no se encuentra en muchos de los modelos que se obtuvieron como resultado.

```{r echo=FALSE}
mod_sulf_vacia <- lm(formula= water$Sulfate~1, water)
mod_sulf_completa <- lm(formula = water$Sulfate~., water)
mod_sulfate <- step(mod_sulf_vacia, scope = list(lower=mod_sulf_vacia, upper=mod_sulf_completa),
                   direction = "forward", trace = FALSE)
summary(mod_sulfate)
```

La regresión realizada para la variable "Solids" tiene un R-squared de 0.037. Tiene cinco variables predictoras como "Sulfate", "ph", "Chloramines", "Hardness" y "Potability" cuyos p-valores indican que son significativamente diferentes de cero. A diferencia del anterior modelo, aquí se incluye la potabilidad del agua, pero puede observarse que al tomar un nivel de 0.05 se llegaría a la conclusión de que está variable es cero y no sería relevante para la regresión. Así, se observa que aunque está variable antes del análisis se pensaba que explicaría gran parte de los datos, en los modelos no se cumple esta hipótesis.

```{r echo=FALSE}
mod_solid_vacia <- lm(formula= water$Solids~1, water)
mod_solid_completa <- lm(formula = water$Solids~., water)
mod_solids <- step(mod_solid_vacia, scope = list(lower=mod_solid_vacia, upper=mod_solid_completa),
                     direction = "forward", trace = FALSE)
summary(mod_solids)
```

Adicionalmente se realizó un modelo de regresión para la potabilidad del agua. Dado que esta variable es cualitativa, se utilizó regresión logística, la cual usa a dicha variable categórica como dependiente. Primero se quiso hacer un modelo con todas las variables, en donde se observó que parecía que las variables "solids" y "organic_carbon" eran las más significativas.
```{r}
mod = glm(formula = Potability ~ ph + Hardness + Solids + Chloramines
          + Sulfate + Conductivity +
            Organic_carbon + Trihalomethanes + Turbidity,
          data = water, family = 'binomial')
summary(mod)
```

Tras varios modelos de prueba finalmente se realizó uno con solo estas dos variables mencionadas anteriormente para ver su significancia con la potabilidad, y se pudo observar que estas dos tienen una significancia similar, siendo "solids" la más importante con un p-valor de 0.05, y "Organic_carbon" con un p-valor de 0.08. Lo cual tiene sentido, teniendo en cuenta lo que representan esas dos variables en el conjunto de datos.
```{r}
mod_final = glm(formula = Potability ~ Solids + Organic_carbon, data = water,
                family = 'binomial')
summary(mod_final)
```

Comparando ambos valores de criterio de información de Akaike (AIC), vemos que el segundo modelo es un poco más recomendable que el primero, pues dicho valor es algo menor.

Finalmente, se estudió si las variables predictoras de los modelos de regresión eran las mismas en los diferentes grupos de muestras (agua potable y no potable). Cabe resaltar que el valor de R-squared de estos modelos es bajo, pero mejor que los anteriores. Existen algunos casos donde las variables predictoras son las mismas, y otros que presentan algunas diferencias importantes. Para ilustrar esto, se tomó la variable "Sulfate", ya que es donde se presentan las mayores diferencias.

Para las observaciones de agua no potable las variables predictoras son "ph", "Hardness" y "Solids". Este modelo explica un 6% de la variabilidad de los datos, aproximadamente, lo que es mayor a los modelos explicados anteriormente.

```{r echo=FALSE}
################# Modelo Sulfate con potabilidad 0 ##################
mod_sulf_vacia0 <- lm(formula= pob0$Sulfate~1, pob0)
mod_sulf_completa0 <- lm(formula = pob0$Sulfate~., pob0)
mod_sulf0 <- step(mod_sulf_vacia0, scope = list(lower=mod_sulf_vacia0,
                                                upper=mod_sulf_completa0),
                   direction = "forward", trace = FALSE)
summary(mod_sulf0)
```

Por otra parte, para el modelo de "Sulfate" con agua potable se tienen las variables "Trihalomethanes", "Chloramines" y "Organic carbon" como adicionales a las del anterior modelo. El R-squared de modelo es 0.14, lo cual es un valor mucho más alto que todos los obtenidos anteriormente. Las variables adicionales en esta regresión pueden deberse a que los compuestos como las cloraminas y los trihalometanos se encuentran presentes en agua que ha sido tratada y desinfectada para mejorar sus características. Por esta razón tiene sentido que no se encuentren en la regresión realizada para las observaciones no potables. 

```{r echo=FALSE}
# ################# Modelo Sulfate con potabilidad 1 ##################
mod_sulf_vacia1 <- lm(formula= pob1$Sulfate~1, pob1)
mod_sulf_completa1 <- lm(formula = pob1$Sulfate~., pob1)
mod_sulf1 <- step(mod_sulf_vacia1, scope = list(lower=mod_sulf_vacia1
                                                , upper=mod_sulf_completa1),
                   direction = "forward", trace = FALSE)
summary(mod_sulf1)
```

## Análisis de resultados 

- Los modelos de regresión lineal obtenidos para esta base de datos no tienen un buen desempeño en general. El porcentaje de la variabilidad que se explicaba era muy bajo, y no pueden considerarse buenos para hacer predicciones de nuevas observaciones.
- Las variables "Conductivity" y "Turbidity" son las menos significativas dentro de los modelos, ya que casi ninguna de las regresiones que se construyeron contenían estas variables. Así mismo, la variable "Potability" que es la más relevante de la base de datos, no tiene mucha correlación con las otras variables, y contrario a lo que se pensó no explicaba casi nada de las demás variables.
- Al realizar regresiones separando los grupos de observaciones en Agua potable y no potable se encontró que algunas variables se encuentran presentes en la mayoría de los modelos para el grupo potable, por ejemplo las variables "Chloramines" y "Trihalomethanes". Además, algunas variables como "ph", "Hardness", "Solids", entre otras están presentes en ambos modelos, lo que indica que no son exclusivas de un solo grupo.



# 2. Análisis de componentes principales

## Procedimiento y resultados

Inicialmente se removió la variable "potability" debido a que esta era cualitativa, posteriormente se construye la matriz de varianzas y covarianzas $\textbf{S}$ y se encuentran sus vectores y valores propios $\lambda_{S_i}$ para así determinar cuáles componentes son más relevantes para la explicación del modelo. Se encuentra que se logra una proporción del $99.9$ % solo con $\lambda_{S_{1}}$.
Luego se buscan las correlaciones de las demás componentes con $\lambda_{S_1}$ obteniendo los siguientes valores:

- $\rho_{X_{1}} = -0.055$ \
- $\rho_{X_{2}} = -0.0014$ \
- $\rho_{X_{3}} = 0.0001$ \
- $\rho_{X_{4}} = -0.044$ \
- $\rho_{X_{5}} = -0.004$ \
- $\rho_{X_{6}} = 0.00017$ \
- $\rho_{X_{7}} = 0.003$ \
- $\rho_{X_{8}} = -0.0005$ \
- $\rho_{X_{9}} = 0.025$ \

Se realizó un proceso análogo para los datos estandarizados, obteniendo:

1. Para explicar el $81.4$ % se requieren 7 componentes. 

2. Índices de correlación relacionados a $\lambda_{Z_i}$:

  - $\rho_{X_{1}} = -0.034$ \
  - $\rho_{X_{2}} = 0.011$ \
  - $\rho_{X_{3}} = 0.733$ \
  - $\rho_{X_{4}} = -0.278$ \
  - $\rho_{X_{5}} = -0.642$ \
  - $\rho_{X_{6}} = 0.086$ \
  - $\rho_{X_{7}} = -0.115$ \
  - $\rho_{X_{8}} = 0.0095$ \
  - $\rho_{X_{9}} = 0.178$ \
  
Finalmente, se calculó la proporción de la primera y primeras dos componentes no estandarizadas, sin tomar en cuenta la variable "Solids", resultando en $71.1$ % y $85.8$ % respectivamente.

```{r}
basewop = base100 
basewop$Potability <- NULL 
######################################################
#No Estandarizado
######################################################
S = cov(basewop) 
E = eigen(S)

Values  = E$values  
Vectors = E$vectors 

prop = sum(Values[1])/sum(Values) 

rhox1 = (Vectors[1,1]*sqrt(Values[1]))/S[1,1]
rhox2 = (Vectors[2,1]*sqrt(Values[1]))/S[2,2]
rhox3 = (Vectors[3,1]*sqrt(Values[1]))/S[3,3]
rhox4 = (Vectors[4,1]*sqrt(Values[1]))/S[4,4]
rhox5 = (Vectors[5,1]*sqrt(Values[1]))/S[5,5]
rhox6 = (Vectors[6,1]*sqrt(Values[1]))/S[6,6]
rhox7 = (Vectors[7,1]*sqrt(Values[1]))/S[7,7]
rhox8 = (Vectors[8,1]*sqrt(Values[1]))/S[8,8]
rhox9 = (Vectors[9,1]*sqrt(Values[1]))/S[9,9] 

######################################################
#Estandarizado
######################################################
R = cov2cor(S) 
Ez = eigen(R)

Valuesz  = Ez$values 
Vectorsz = Ez$vectors 

propz = sum(Valuesz[1:7])/9 

rhox1z = (Vectorsz[1,1]*sqrt(Valuesz[1]))/R[1,1]
rhox2z = (Vectorsz[2,1]*sqrt(Valuesz[1]))/R[2,2]
rhox3z = (Vectorsz[3,1]*sqrt(Valuesz[1]))/R[3,3]
rhox4z = (Vectorsz[4,1]*sqrt(Valuesz[1]))/R[4,4]
rhox5z = (Vectorsz[5,1]*sqrt(Valuesz[1]))/R[5,5]
rhox6z = (Vectorsz[6,1]*sqrt(Valuesz[1]))/R[6,6]
rhox7z = (Vectorsz[7,1]*sqrt(Valuesz[1]))/R[7,7]
rhox8z = (Vectorsz[8,1]*sqrt(Valuesz[1]))/R[8,8]
rhox9z = (Vectorsz[9,1]*sqrt(Valuesz[1]))/R[9,9]


######################################################
#No Estandarizado - Sin solids 
######################################################
basewop2 = base100 
basewop2$Potability <- NULL 
basewop2$Solids <- NULL 

S2 = cov(basewop2) 
E2 = eigen(S2) 

Values2  = E2$values 
Vectors2 = E2$vectors 

prop21 = sum(Values2[1])/sum(Values2)
prop2 = sum(Values2[1:2])/sum(Values2) 
```
# Análisis de Resultados

- Observando los resultados se puede resaltar que la primera componente es capaz de explicar el $99.9$ % de los datos, esto puede ser causado por una gran relación entre las variables, o a que algunas de estas tengan un peso superior a las demás. Para determinar la situación, se observa la matriz de correlación $\textbf{S}$, en la cual se puede evidenciar que la variable "Solids" en efecto posee un mayor peso debido a su varianza, por lo tanto, es coherente que explique tal cantidad de datos.

- Debido a la proporción explicada por la variable "Solids" se encontró pertinente contrastar estos resultados con los datos estandarizados. La notable disminución en la proporción indica que la varianza de la variable "Solids" es un elemento importante para poder aplicar PCA, ya que sin esta la reducción de dimensionalidad no vale la pena. Esto también se puede comprobar al momento de considerar un caso no estandarizado sin esta variable, donde, aunque las proporciones son mejores que en el estandarizando, se pierde una cantidad notable de información.

- Los bajos valores de los índices de correlación correspondientes a $\lambda_{S_1}$ y $\lambda_{Z_1}$ sugieren que las variables no presentan una relación notable entre sí.

<div style="page-break-after: always;"></div>

# 3. Clasificación
## Normalidad
Al realizar histogramas a todas las columnas con valores cuantitativos se puede comprobar que son aproximadamente normales.

```{r}
hist.data.frame(base100)
```

## Procedimiento y resultados 

Se particionaron los datos en grupos de entrenamiento y prueba con una proporción 80-20 respectivamente, las clasificaciones se dan de la siguiente manera:

- $\pi_{1}$: la muestra no es potable. \
- $\pi_{2}$: la muestra es potable. \


$\phantom{Odio AED aaaaaaaaaaaaa}$ |**Caso 1**|**Caso 2**|**Caso 3**|**Caso 4**
:-----:|:-----:|:-----:|:-----:|:-----:
Costo 12|1|1|1|1
Costo 21|1|2|5|10
Clasificación $\pi_1$|509|592|630|639
Aciertos $\pi_1$|357|389|402|405
Errores $\pi_1$|152|203|228|236
Clasificación $\pi_2$|147|64|26|19
Aciertos $\pi_2$|91|45|20|14
Errores $\pi_2$|56|19|6|3
**Poporción Aciertos** $\pi_1$|**70.14%**|**65.71%**|**63.81%**|**63.38%**
**Poporción Errores $\pi_1$ **|**29.86%**|**34.29%**|**36.19%**|**36.93%**
**Poporción Aciertos $\pi_2$**|**61.90%**|**70.31%**|**76.92%**|**73.68%**
**Poporción Errores $\pi_2$**|**38.10%**|**29.69%**|**23.08%**|**15.79%**
**Poporción Aciertos Total**|**68.29%**|**66.16%**|**64.33%**|**63.68%**
**Poporción Errores Total**|**31.71%**|**33.84%**|**35.67%**|**36.32%**

```{r}


mean100 = colMeans(base100) 

base80 = sample_n(base100, 2620) 

mean80 = colMeans(base80) 

base20 = anti_join(base100, base80) 
base20c = base20 

mean20 = colMeans(base20) 

#####################
#Costos Y Probabilidad 
#####################

p2 = mean80[10] 
p1 = 1 - p2 

#####################
#Covarianza y medias
#####################

base80s = split(base80, f = base80$Potability) 

base80_0 = base80s[["0"]] 
base80_1 = base80s[["1"]] 

base80_0$Potability <- NULL 
base80_1$Potability <- NULL
base20$Potability <- NULL  
S1 = cov(base80_0)   
S2 = cov(base80_1) 
m1 = colMeans(base80_0) 
m2 = colMeans(base80_1) 

m1 = t(m1)
m1 = t(m1)
m2 = t(m2)
m2 = t(m2) 
#####################
#Clasificación 
#####################

k = (1/2)*log((det(S1))/(det(S2)))+(1/2)*(t(m1)%*%solve(S1)%*%m1 
                                          - t(m2)%*%solve(S2)%*%m2) 


#####################
#costos iguales 
#####################

c12 = 1 
c21 = 1  

n1 = 0 
n1b = 0 
n1m = 0 
n2 = 0 
n2b = 0 
n2m = 0 

for (i in 1:656){ 
  x0 = t(base20[i,])
  if ((-1/2)*t(x0)%*%
      (solve(S1) - solve(S2))%*%x0 + (t(m1) %*%
                                      solve(S1) - t(m2) %*% solve(S2))%*%
      x0 - k >= log((c12*p2)/(c21*p1))){
    
    n1 = n1 + 1
    if (base20c[i,10] == 0){
      n1b = n1b + 1
    }else{
      n1m = n1m + 1
    }
    
  } else {
    n2 = n2 + 1
    if (base20c[i,10] == 1){
      n2b = n2b + 1
    }else{
      n2m = n2m + 1 
    }
  }
} 


#####################
#costos diferentes 1 
#####################

c12 = 1
c21 = 2

n1 = 0 
n1b = 0 
n1m = 0 
n2 = 0 
n2b = 0 
n2m = 0 

for (i in 1:656){ 
  x0 = t(base20[i,])
  if ((-1/2)*t(x0)%*%(solve(S1) - solve(S2))%*%
      x0 + (t(m1) %*% solve(S1) - t(m2) %*%
            solve(S2))%*%x0 - k >= log((c12*p2)/(c21*p1))){
    
    n1 = n1 + 1
    if (base20c[i,10] == 0){
      n1b = n1b + 1
    }else{
      n1m = n1m + 1
    }
    
  } else {
    n2 = n2 + 1
    if (base20c[i,10] == 1){
      n2b = n2b + 1
    }else{
      n2m = n2m + 1 
    }
  }
}  


#####################
#costos diferentes 2 
#####################

c12 = 1 
c21 = 5 

n1 = 0 
n1b = 0 
n1m = 0 
n2 = 0 
n2b = 0 
n2m = 0 

for (i in 1:656){ 
  x0 = t(base20[i,])
  if ((-1/2)*t(x0)%*%(solve(S1) - solve(S2))%*%
      x0 + (t(m1) %*% solve(S1) - t(m2) %*% 
            solve(S2))%*%x0 - k >= log((c12*p2)/(c21*p1))){
    
    n1 = n1 + 1
    if (base20c[i,10] == 0){
      n1b = n1b + 1
    }else{
      n1m = n1m + 1
    }
    
  } else {
    n2 = n2 + 1
    if (base20c[i,10] == 1){
      n2b = n2b + 1
    }else{
      n2m = n2m + 1 
    }
  }
} 


#####################
#costos diferentes 3 
#####################

c12 = 1 
c21 = 10

n1 = 0 
n1b = 0 
n1m = 0 
n2 = 0 
n2b = 0 
n2m = 0 

for (i in 1:656){ 
  x0 = t(base20[i,])
  if ((-1/2)*t(x0)%*%(solve(S1) - solve(S2))%*%
      x0 + (t(m1) %*% solve(S1) - t(m2) %*% 
            solve(S2))%*%x0 - k >=log((c12*p2)/(c21*p1))){
    
    n1 = n1 + 1
    if (base20c[i,10] == 0){
      n1b = n1b + 1
    }else{
      n1m = n1m + 1
    }
    
  } else {
    n2 = n2 + 1
    if (base20c[i,10] == 1){
      n2b = n2b + 1
    }else{
      n2m = n2m + 1 
    }
  }
} 

```

## Análisis de resultados
Se puede notar que al aumentar el coste de los errores del tipo $\pi_2$ estos se reducen drásticamente, a costa de que los errores $\pi_1$ aumenten, sin embargo, esta es aceptable puesto que es preferible clasificar agua potable como no potable, que agua no potable como potable, puesto que en el caso contrario se podrían generar graves problemas de salud.

# 4. Conclusiones.
- Viendo el desempeño de los modelos realizados no se puede decir que estos sean optimos para un uso práctico, es decir, las variables presentes no necesariamente hablan de la potabilidad de una muestra dada.
- Se puedo evidenciar que las varaibles seleccionadas no presentan una correlacion notable entre si, lo que influyó negativamente en el desempeño de los modelos, dentro de las cuales destacan "Solids", "Turbidity" y "Conductivity"
- Finalmente se puede concluir que es pertinente realizar un nuevo estudio teniendo en cuenta otros parametros que puedan explicar mejor la potabilidad de la muestra, para asi aumentar el desempeño de los modelos propuestos.




