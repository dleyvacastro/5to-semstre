# Punto 3
**a)**
Es simetrica, puesto que es igual a su transpuesta. \
**b)**
$$
	\left|
	\begin{bmatrix}
		9 & -2 \\
		-2 & 6
	\end{bmatrix} 
	- 
	\lambda
	\begin{bmatrix}
		1 & 0 \\
		0 & 1
	\end{bmatrix} 
	\right| = 
	\begin{vmatrix}
		9-\lambda & -2 \\
	-2 & 6- \lambda
	\end{vmatrix} = 
	(9 - \lambda)(6- \lambda) - 4 = 0 \\
$$
$$\lambda^{2} -15\lambda +50 = 0$$
$$\lambda_{1} = 5 \quad \lambda_2 = 10$$
Es semi definida positiva ya que los valores propios son positivos.
\
**c)**
\begin{aligned}
	\begin{bmatrix}
		9 & -2 \\
		-2 & 6
	\end{bmatrix}
	\begin{bmatrix}
		x_1 \\
		x_2
	\end{bmatrix} = 5 
	\begin{bmatrix}
		\lambda_1 \\
		\lambda_2
	\end{bmatrix}
	\quad 
	\begin{matrix}
		9x_1 -2x_2 = 5 x_1 \\
		-2x_1 +6x_2 = 5x_2
	\end{matrix}
	\longleftrightarrow x_2=2x_1
\end{aligned}

\begin{aligned}
	\begin{bmatrix}
		9 & -2 \\
		-2 & 6
	\end{bmatrix}
	\begin{bmatrix}
		x_1 \\
		x_2
	\end{bmatrix} = 10
	\begin{bmatrix}
		\lambda_1 \\
		\lambda_2
	\end{bmatrix}
	\quad 
	\begin{matrix}
		9x_1 -2x_2 = 10 x_1 \\
		-2x_1 +6x_2 = 10 x_2
	\end{matrix}
	\longleftrightarrow x_2=\frac{-x_1}{2}
\end{aligned}
Arbitrariamente $x_1 = 1$
$$e_1' = \left[\frac{1}{\sqrt{3}}, \frac{2}{\sqrt{3}} \right] \quad e_2' = \left[\frac{2}{\sqrt{5}}, -\frac{1}{\sqrt{5}} \right]$$

**d)**
\
\begin{aligned}
	E[A] & = 5 \begin{bmatrix}
		\frac{1}{\sqrt{3}} \\ \frac{2}{\sqrt{3}} 
		\end{bmatrix} \begin{bmatrix}
		\frac{1}{\sqrt{3}} & \frac{2}{\sqrt{3}} 
		\end{bmatrix}
		+
		10\begin{bmatrix}
		\frac{2}{\sqrt{5}} \\ \frac{-1}{\sqrt{5}} 
		\end{bmatrix} \begin{bmatrix}
		\frac{2}{\sqrt{5}} & \frac{-1}{\sqrt{5}} 
		\end{bmatrix} \\
		& = \begin{bmatrix}
			\frac{29}{3} & -\frac{2}{3} \\
			-\frac{2}{3} & \frac{26}{3}
		\end{bmatrix}
\end{aligned}
**e)**
$$|A| = 54 -4 = 50$$
\begin{aligned}
	A^{-1} & = \frac{1}{50} \begin{pmatrix}
							6 & 2 \\
							2 & 9
						\end{pmatrix} \\
		& = \begin{pmatrix}
			\frac{3}{25} & \frac{1}{25} \\
			\frac{1}{25} & \frac{9}{50}
		\end{pmatrix}
\end{aligned}

**f)**
\begin{aligned}
\begin{vmatrix}
	\dfrac{3}{25} - \lambda & \dfrac{1}{25} \\
	\dfrac{1}{25} & \dfrac{9}{50} - \lambda
\end{vmatrix} =
\left( \frac{3}{25} - \lambda \right) \left( \frac{9}{50} - \lambda \right) - \frac{1}{625}
\end{aligned}
$$\lambda^2 -\frac{3}{10}\lambda + \frac{1}{50}$$
$$\lambda_1 = 0, 1 = \frac{1}{10} \quad \lambda_2 = 0, 2 = \frac{2}{10}$$
\begin{aligned}
\begin{bmatrix}
	\dfrac{3}{25} & \dfrac{1}{25} \\
	\dfrac{1}{25} & \dfrac{9}{30}
\end{bmatrix}
\begin{bmatrix}
	x_1 \\ 
	x_2
\end{bmatrix}
 = \frac{1}{10} \begin{bmatrix} x_1 \\ x_2 \end{bmatrix}
\begin{matrix}
	\frac{3}{25} x_1 + \frac{1}{25}x_2 = \frac{1}{10} x_1 \\
	\frac{1}{25}x_1 + \frac{9}{50}x_2 = \frac{1}{10} x_2  
\end{matrix}
\end{aligned}
Todos los vectores cumplen que $x_1 = -2x_2$

# Punto 4
$$\textbf{V}^{1/2}\rho\textbf{V}^{1/2} = \Sigma $$
\begin{aligned}
	\left[
		\begin{pmatrix}
			\sqrt{\sigma_{11}} & \cdot & \cdot  & \cdot & 0 \\
			\cdot & \cdot & & & \cdot \\
			\cdot & & \cdot & & \cdot \\
			\cdot & & & \cdot & \cdot \\
			0 & \cdot & \cdot  & \cdot & \sqrt{\sigma_{nn}} \\
		\end{pmatrix}
	\cdot
		\begin{pmatrix}
			1 & \cdot & \cdot  & \cdot & \dfrac{\sigma_{1n}}{\sqrt{\sigma_{11}}\sqrt{\sigma_{nn}}} \\
			\cdot & \cdot & & & \cdot \\
			\cdot & & \cdot & & \cdot \\
			\cdot & & & \cdot & \cdot \\
			\dfrac{\sigma_{1n}}{\sqrt{\sigma_{11}}\sqrt{\sigma_{nn}}} & \cdot & \cdot  & \cdot & 1 \\
		\end{pmatrix}
	\right] \cdot
		\begin{pmatrix}
			\sqrt{\sigma_{11}} & \cdot & \cdot  & \cdot & 0 \\
			\cdot & \cdot & & & \cdot \\
			\cdot & & \cdot & & \cdot \\
			\cdot & & & \cdot & \cdot \\
			0 & \cdot & \cdot  & \cdot & \sqrt{\sigma_{nn}} \\
		\end{pmatrix} 
	
\end{aligned} 
\
\begin{aligned}
	& = \begin{pmatrix}
			\sqrt{\sigma_{11}} & \cdot & \cdot  & \cdot & \dfrac{\sigma_{n1}}{\sqrt{\sigma_{nn}}} \\
			\cdot & \cdot & & & \cdot \\
			\cdot & & \cdot & & \cdot \\
			\cdot & & & \cdot & \cdot \\
			\dfrac{\sigma_{1n}}{\sqrt{\sigma_{11}}} & \cdot & \cdot  & \cdot & \sqrt{\sigma_{nn}} \\
	\end{pmatrix} \cdot \begin{pmatrix}
			\sqrt{\sigma_{11}} & \cdot & \cdot  & \cdot & 0 \\
			\cdot & \cdot & & & \cdot \\
			\cdot & & \cdot & & \cdot \\
			\cdot & & & \cdot & \cdot \\
			0 & \cdot & \cdot  & \cdot & \sqrt{\sigma_{nn}} \\
	\end{pmatrix} \\ \\
	& = \begin{pmatrix}
			\sigma_{11} & \cdot & \cdot  & \cdot & \sigma_{n1} \\
			\cdot & \cdot & & & \cdot \\
			\cdot & & \cdot & & \cdot \\
			\cdot & & & \cdot & \cdot \\
			\sigma_{1n} & \cdot & \cdot  & \cdot & \sigma_{nn} \\
		\end{pmatrix} \\
	& = \Sigma
\end{aligned}

Note que $V^{1/2}$ tiene elementos unicamente en la diagonal, y ya que estos elementos son positivos, podemos decir que la matriz es invertible, por ende: 
\begin{aligned}
	V^{1/2}\rho V^{1/2} & = \Sigma \\
	\rho V^{1/2} & = (V^{1/2})^{-1} \Sigma \\
	\rho & = (V^{1/2})^{-1} \Sigma (V^{1/2})^{-1}
\end{aligned}
# Punto 5
**a)**
$X_1 - 2X_2$ \
$E[a] = E[X_1] - 2E[X_2]$ \
$var(a) = var(X_1) - 4 var(X_2) + 2 cov(X_1, X_2)$ \
\
**b)**
$-X_1+3X_2$ \
$E[b] =-E[X_1] + 3E[X_2]$  \
$var(b) =var(X_1) + 9var[X_2] + 2 cov(X_2,X_1)$ \
\
**c)**
$X_1 + X_2 +X_3$ \
$E[c] = \sum_{i = 1}^{3} E[X_i]$ \
$var(c) = \sum_{i = 1}^{3} var(X_i) + 2cov(X_1, X_2) + 2cov(X_2, X_3)+ 2cov(X_1, X_3)$ \
\
**d)**
$X_1 + 2X_2 - X_3$ \
$E[d] = E[X_1] + 2E[X_2] - E[X_3]$ \
$var(d) = var(X_1) + 4var(X_2) +var(X_3)+2cov(X_1, X_2) + 2cov(X_1, X_3) + 2cov(X_2, X_3)$ \
\
**e)**
$3X_1 - 4X_2$ \
$E[e] = 3E[X_1] - 4E[X_2]$ \
$var(e) = 9var(X_1) +16var(X_2)$ 


# Punto 6

**a)** 

**b)**

**c)**
Sabemos que $\cos(\theta) = \frac{1}{2}$, podemos decir que:
$$R = 
\begin{bmatrix}
1 & -\frac{1}{2} \\
- \frac{1}{2} & 1 
\end{bmatrix}
$$
ahora teniendo que $\sqrt{s_{11}} = 4$, $\sqrt{s_{22}} = 1$
decimos que: 
$$
S = \begin{bmatrix}
4^2 & - \frac{1}{2} \cdot 4 \cdot-1 \\
- \frac{1}{2} \cdot 4 \cdot-1  & 1^2
\end{bmatrix} = \begin{bmatrix}
16 & -2 \\
-2 & 1
\end{bmatrix}
$$
**d)**
$$|S| = 16-4 = 12$$

# Punto 7
Cuando estandarizamos todas nuestras mediciones, las medidas de los vectores quedan todas iguales además de cambiar su dirección, esto último gracias a dividir por la desviación estándar. Por lo tanto, gráficamente cuando se estandariza, el volumen de el espacio que forman las varianzas tiende a cambiar. Por lo tanto, si yo a estas estandarizaciones las multiplicó por sus respectivas varianzas, se podría decir que vuelven a tener su dirección original, por lo cual $|S| = (s, s, s....)|R|$

# Punto 8
\begin{aligned}
E(\textbf{VV'}) & = \Sigma_{V} + \mu_{V}\mu'_{V} \\
\Sigma_{V} & =  E(\textbf{VV'}) - E(V)E(V)' \\
\end{aligned}
$E(\textbf{VV'}) \rightarrow$ Valor esperado seguindo momento. \
$E(V)E(V)' \rightarrow$ Valor esperado primer momento al cuadrdo. \
$\Sigma_{V} \rightarrow$ Matriz de varianzas y covarianzas.

# Punto 9 
**a)**
\begin{aligned}
f(x) = \frac{1}{2\pi \cdot 0.88}e^{-\left( x - \begin{pmatrix}
1 \\ 3 \end{pmatrix} \right)' \cdot \Sigma^{-1} \cdot \frac{\left(x - \begin{pmatrix} 1 \\ 3 \end{pmatrix} \right)}{2}}
\end{aligned}
**b)**
\begin{aligned}
	\begin{pmatrix}
		\begin{pmatrix} x_1 \\ x_2 \end{pmatrix} - \begin{pmatrix} 1 \\ 3 \end{pmatrix}
	\end{pmatrix}' \begin{pmatrix} 2 & -\frac{11}{10} \\ -\frac{11}{10} & 1 \end{pmatrix} \begin{pmatrix}
		\begin{pmatrix} x_1 \\ x_2 \end{pmatrix} - \begin{pmatrix} 1 \\ 3 \end{pmatrix}
	\end{pmatrix}
\end{aligned}
\begin{aligned}
	= \begin{pmatrix} x_1-1 & x_2 -3\end{pmatrix}\begin{pmatrix}
	 											1.2 & 1.4 \\
												1.4 & 2.5
												\end{pmatrix}
	\begin{pmatrix} x_1 -1 \\ x_2-3\end{pmatrix}
\end{aligned}

$$= \left( 1.2 x_1 +1.4 x_2 -5.4 \quad 1.4x_1 +2.5x_2 - 8.4 \right) \begin{pmatrix} x_1 -1 \\ x_2-3\end{pmatrix}$$

$$= 1.2x_{1}^{2} -1.2x_1 +1.4 x_1 x_2 -1.4 x_2 -5.4 x_1 +5.4$$ $$+1.4x_1 x_2 -4.2 x_1 +2.5 x_{2}^{2}-7.5 x^{2} -8.9x_{2} +26.7$$

$$= 1.2 x_{1}^{2}-10.8 x_1 +2.8 x_1 x_2 -17.8 x_2 +2.5 x_{2}^{2} +32.1$$

# Punto 10
**a)** No, puesto que $\Sigma_{12} \neq 0$ \
**b)** Si, Puesto que $\Sigma_{23} = 0$ y $\textbf{X}$ es normal.
