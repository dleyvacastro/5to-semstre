import serial

read = serial.Serial('/dev/ttyACM0', 115200)
result = []

for i in range(50):
    print(f'-------------{i+1}-------------')
    sr = read.readline()
    sr = sr.decode('utf-8')
    print(sr)
    if sr != '':
        result.append(sr)
    sr = sr.split(',')
    del sr[0]
    print(sr)
    # print('Hora: {0}\nIndicacion : {1}\nLongitud: {2}\n Norte: {3}\n Latitud: {4}\nOeste: {5}'.format(*sr))
    # result.append(read.readline())

with open('results.txt', 'w') as f:
    f.write('\n'.join(result))
read.close()
