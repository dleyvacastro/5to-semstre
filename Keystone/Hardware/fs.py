#!/usr/bin/python3

import firebase_admin
from random import uniform, randint
from firebase_admin import credentials
from firebase_admin import firestore
from google.cloud.firestore import GeoPoint
from datetime import datetime, timezone

def generate_status() -> str:
    if randint(0,1):
        return "Safe"
    return "Missing"

def generate_coords() -> dict:
    return {"latitude": uniform(-90,90), "longitude": uniform(-180,180)}

def filldb() -> dict:
    data = {
        float(k) : {
            u"Device_name": f"Prototype {k}",
            u"Status":generate_status(),
            u"Actual_position":GeoPoint(**generate_coords()),
            u"Historic":{
                f"{i}":{
                    "Position":GeoPoint(**generate_coords()),
                    "Date":datetime.now(tz=timezone.utc)
                }for i in range(1, randint(1,10))
                # f"TS {j}":generate_coords() for j in range(randint(1,10))
            }
        } for k in range(1, 5)
    }
    for i in data:
        db.collection(u'GPS').document(f'{i}').set(data[i])   

    return data

def get_info(idd: float, parameter : str):
    doc_ref = db.collection(u"GPS").document(f"{idd}")
    doc = doc_ref.get()
    if doc.exists:
        if parameter != "full":
            if "Historic" not in doc.to_dict() and parameter == "Historic":
                return {}
            return doc.to_dict()[f"{parameter}"]
        return doc.to_dict()
    return False

def update_parameter(idd: float, parameter : str, value):
    data = get_info(idd, "full")
    data[parameter] = value
    db.collection(u"GPS").document(f"{idd}").set(data)

    

def parser(idd : float, latitude : float, longitude : float) -> dict:
    status = get_info(idd, "Status")
    
    if status:
        update_parameter(idd, "Actual_position", GeoPoint(latitude= latitude, longitude = longitude))
        if status == "Missing":
            print("adding")
            Hist = get_info(idd, "Historic")

            Hist[f"{len(Hist)+1}"] = {
                u"Position": GeoPoint(latitude= latitude, longitude = longitude),
                u"Date": datetime.now(tz=timezone.utc)
            }
            update_parameter(idd, "Historic", Hist)

    else:
        data = {
            u"Device_name": f"Prototype {idd}",
            u"Status": "Safe",
            u"Actual_position": GeoPoint(latitude= latitude, longitude = longitude),
        }
        db.collection(u"GPS").document(f"{idd}").set(data)


# Use a service account
cred = credentials.Certificate('auth.json')
firebase_admin.initialize_app(cred)

db = firestore.client()

# filldb()

update_parameter(3.0,"Status","Missing")
update_parameter(4.0,"Status","Missing")

