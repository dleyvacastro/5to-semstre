#!/usr/bin/python3
from firebase import firebase
import json
from random import randint, uniform
from datetime import datetime, timezone
from google.cloud.firestore import GeoPoint
# import cgi

def generate_status() -> str:
    if randint(0,1):
        return "Safe"
    return "Missing"

def generate_coords() -> dict:
    return {"latitude": uniform(-90,90), "longitude": uniform(-180,180)}

def filldb():
    data = {
        k : {
            u"Device_name": f"Prototype {k}",
            u"Status":generate_status(),
            u"Actual_position":GeoPoint(**generate_coords()),
            u"Historic":{
                f"{i}":{
                    "posicion":GeoPoint(**generate_coords()),
                    "Fecha":datetime.now(tz=timezone.utc)
                }for i in range(randint(1,10))
                # f"TS {j}":generate_coords() for j in range(randint(1,10))
            }
        } for k in range(1, 5)
    }
    print(data)
    with open("data2.json", 'w') as f:
        f.write(json.dumps(data, indent= 4))
    # firebase.put('/', 'GPS/', [None,  *data])


if __name__ == '__main__':

    firebase = firebase.FirebaseApplication('https://test-key-76e27-default-rtdb.firebaseio.com/', None)
    with open("data.json", 'r') as f:
        f = json.loads(f.read())
        # print(f)
    # print(firebase.get('/', 'Usernames'))
    filldb()
    firebase.put('/', 'data/', f)


