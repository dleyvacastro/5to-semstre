#!/usr/bin/python3
import firebase_admin
import json
from random import uniform, randint
from firebase_admin import credentials
from firebase_admin import firestore
from google.cloud.firestore import GeoPoint
from datetime import datetime, timezone

with open("vets.json") as f:
    data = json.loads(f.read())
    for i in data:
        i["ubication"] = GeoPoint(latitude= i["Latitud"], longitude = i["Longitud"])
        # i["pfp"] = "https://firebasestorage.googleapis.com/v0/b/pawpaw-9d57b.appspot.com/o/profile_assets%2Ffoundations%2Ffund_3.jpg?alt=media&token=05de5327-82b4-421c-ad70-c7842c38619a"
        i["pfp"] = "https://firebasestorage.googleapis.com/v0/b/pawpaw-9d57b.appspot.com/o/profile_assets%2Fvets%2Fvet7.jpg?alt=media&token=5707931c-25e2-4d30-b5ce-839a3db6c4db"
        del i["Latitud"]
        del i["Longitud"]
    print(data)

cred = credentials.Certificate('auth_paw.json')
firebase_admin.initialize_app(cred)

db = firestore.client()

for i in range(len(data)):
    db.collection(u'vets').document(f'{i+1}').set(data[i])   

